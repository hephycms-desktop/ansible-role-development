Development
=========

Install various development tools

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: desktop
      roles:
         - dietrichliko.development

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
